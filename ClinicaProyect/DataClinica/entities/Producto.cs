﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.entities
{
    public class Producto
    {
        public int id { get; set; }             //4
        public string sku { get; set; }         //30 + 3
        public string nombre { get; set; }      //40 + 3
        public string descripcion { get; set; } //60 + 3
        public int cantidad { get; set; }       //4
        public double precio { get; set; }      //8 Total = 180


    }
}
