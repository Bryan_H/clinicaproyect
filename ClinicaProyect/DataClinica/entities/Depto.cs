﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.entities
{
    public class Depto
    {
        public int id { get; set; }             //4
        public string nombre { get; set; }      //30 + 3

        public string codigo { get; set; }      //30 + 3
        public string descripcion { get; set; } //50 + 3
    }
}
