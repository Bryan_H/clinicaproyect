﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.entities
{
    public class ProductoFactura
    {
        public string nombre { get; set; }
        public string sku { get; set; }
        public int cantidad { get; set; }
        public double precio { get; set; }

        public ProductoFactura(string nombre, string sku, int cantidad, double precio)
        {
            this.nombre = nombre;
            this.sku = sku;
            this.cantidad = cantidad;
            this.precio = precio;
        }
    }
}
