﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.entities
{
    public class Empleado
    {
        public int id { get; set; }         //4
        public string nombre { get; set; }  //40 + 3
        public string apellido { get; set; }//40 + 3
        public string telefono { get; set; }//30 + 3
        public string cedula { get; set; }  //32 + 3
        public decimal salario { get; set; } //8
        public string inss { get; set; }    //30 + 3       250


        public override string ToString()
        {            
            return $"Id: {id}, Nombre: {nombre}, Apellido: {apellido}, Telefono: {telefono}, Cedula: {cedula},  Salario: {salario}, INSS: {inss}";
        }        
    }
}
