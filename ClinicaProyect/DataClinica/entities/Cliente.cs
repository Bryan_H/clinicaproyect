﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.entities
{
    public class Cliente
    {
        public int id { get; set; }             //4
        public string nombre { get; set; }      //40 + 3
        public string apellido { get; set; }    //40 + 3
        public string telefono { get; set; }    //30 + 3
        public string cedula { get; set; }      //32 + 3
        public string direccion { get; set; }   //60 + 3 = 250

    }
}
