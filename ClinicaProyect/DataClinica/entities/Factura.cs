﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.entities
{
    public class Factura
    {
        public int id { get; set; }                // 4
        public string cod_factura { get; set; }    //20 + 3
        public long fecha { get; set; }            //8
        public double subtotal { get; set; }       //8
        public double iva { get; set; }            //8
        public double total { get; set; }          //8
        public Empleado empleado { get; set; }     //150
        public Cliente cliente { get; set; }       //150
        public string receta { get; set; }         //50 + 3        

        


    }
}
