﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.entities
{
    class DetalleFactura
    {
        private int id { get; set; }
        private Factura factura { get; set; }
        private Producto producto { get; set; }
        private int cantidad { get; set; }
        private double precio { get; set; }

        public DetalleFactura(int id, Factura factura, Producto producto, int cantidad, double precio)
        {
            this.id = id;
            this.factura = factura;
            this.producto = producto;
            this.cantidad = cantidad;
            this.precio = precio;
        }
    }
}
