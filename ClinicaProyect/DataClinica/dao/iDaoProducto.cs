﻿using DataClinica.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.dao
{
    interface iDaoProducto : iDao<Producto>
    {
        Producto findById(int id);
        Producto findByNombre(string nombre);
        Producto findBySku(string sku);
    }
}
