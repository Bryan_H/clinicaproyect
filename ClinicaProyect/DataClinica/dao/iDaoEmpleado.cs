﻿using DataClinica.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.dao
{
    interface iDaoEmpleado : iDao<Empleado>
    {
        Empleado findById(int id);
        Empleado findByNombre(string nombre);
        Empleado findByCedula(string cedula);
    }
}
