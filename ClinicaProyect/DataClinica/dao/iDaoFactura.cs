﻿using DataClinica.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.dao
{
    interface iDaoFactura : iDao<Factura>
    {
        Factura findById(int id);
        Factura findByCod(string cod);
    }
}
