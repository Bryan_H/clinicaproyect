﻿using DataClinica.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.dao
{
    interface iDaoCliente : iDao<Cliente>
    {
        Cliente findById(int id);
        Cliente findByCedula(string cedula);
    }
}
