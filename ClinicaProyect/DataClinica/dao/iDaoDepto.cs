﻿using DataClinica.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.dao
{
    interface iDaoDepto : iDao<Depto>
    {
        Depto findById(int id);
        Depto findByCodigo(string cod);
    }
}
