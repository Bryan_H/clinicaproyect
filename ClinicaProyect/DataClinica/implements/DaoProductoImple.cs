﻿using DataClinica.dao;
using DataClinica.entities;
using DataClinica.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.implements
{
    public class DaoProductoImple : iDaoProducto
    {
        private BinaryWriter bwHeader;
        private BinaryWriter bwData;
        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 180;

        private List<HeaderIndex> headerIndices;
        private Header header;

        public DaoProductoImple()        {     }

        private void Open()
        {
            try
            {
                headerIndices = new List<HeaderIndex>()
                {
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "IdProducto"
                    },
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "SkuProducto"
                    },
                     new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "NombreProducto"
                    }
                };

                header = new Header()
                {
                    N = 0,
                    Name = "hProducto",
                    HeaderIndices = headerIndices
                };


                fsHeader = new FileStream("hProducto.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("dProducto.data", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if (fsHeader.Length == 0)
                {
                    bwHeader.Write(0);//n
                    bwHeader.Write(0);//k
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }


        public List<Producto> All()
        {
            List<Producto> prods = new List<Producto>();
            try
            {

                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Producto prod = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    prod = new Producto()
                    {
                        id = brData.ReadInt32(),
                        nombre = brData.ReadString(),
                        sku = brData.ReadString(),
                        descripcion = brData.ReadString(),
                        cantidad = brData.ReadInt32(),
                        precio = brData.ReadDouble(),                        
                    };

                    prods.Add(prod);
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return prods;
        }

        public void Create(Producto t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                long pos = k * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++k);
                bwData.Write(t.nombre);
                bwData.Write(t.sku);
                bwData.Write(t.descripcion);
                bwData.Write(t.cantidad);
                bwData.Write(t.precio);                
                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(++n);
                bwHeader.Write(k);

                long hpos = 8 + (n - 1) * 4;
                bwHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                bwHeader.Write(k);

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public bool Delete(Producto t)
        {
            FileStream temporal = new FileStream("temporal3.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);

            try
            {
                BinaryReader reader = new BinaryReader(temporal);
                BinaryWriter writer = new BinaryWriter(temporal);
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                writer.Write(n - 1);
                writer.Write(k);
                int j = 0;

                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int id = brHeader.ReadInt32();
                    if (id == t.id)
                    {
                        continue;
                    }
                    long tmpPos = 8 + 4 * j++;
                    writer.BaseStream.Seek(tmpPos, SeekOrigin.Begin);
                    writer.Write(id);
                }

                Close();
                File.Delete("hProducto.dat");
                writer.Close();
                reader.Close();
                temporal.Close();
                File.Move("temporal3.dat", "hProducto.dat");
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

            return false;
        }

        public Producto findBySku(string sku)
        {
            Producto prod = null;

            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dSku = brData.ReadString();

                    if (!sku.Equals(dSku, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }

                    prod = new Producto()
                    {
                        id = brData.ReadInt32(),
                        nombre = brData.ReadString(),
                        sku = brData.ReadString(),
                        descripcion = brData.ReadString(),
                        cantidad = brData.ReadInt32(),
                        precio = brData.ReadDouble()                      

                    };
                    break;
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return prod;
        }

        public Producto findById(int id)
        {
            Producto prod = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                int index = Finder.BinarySearchById(brHeader, id, 0, n - 1);

                if (index < 0)
                {
                    return prod;
                }

                long dpos = index * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                prod = new Producto()
                {
                    id = brData.ReadInt32(),
                    nombre = brData.ReadString(),
                    sku = brData.ReadString(),
                    descripcion = brData.ReadString(),
                    cantidad = brData.ReadInt32(),
                    precio = brData.ReadDouble()

                };

                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return prod;
        }

        public Producto findByNombre(string nombre)
        {
            throw new NotImplementedException();
        }

        public bool Update(Producto t)
        {
            try
            {
                Open();
                long pos = ((t.id - 1) * SIZE);

                if (pos < 0)
                {
                    return false;
                }

                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(t.id);
                bwData.Write(t.nombre);
                bwData.Write(t.sku);
                bwData.Write(t.descripcion);
                bwData.Write(t.cantidad);
                bwData.Write(t.precio);                

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
            return true;
        }

        public int getGuardados()
        {
            Open();
            brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHeader.ReadInt32();
            int k = brHeader.ReadInt32();
            Close();
            return k;
        }
    }
}
