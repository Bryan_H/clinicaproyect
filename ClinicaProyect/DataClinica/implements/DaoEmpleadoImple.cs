﻿using DataClinica.dao;
using DataClinica.entities;
using DataClinica.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.implements
{
    public class DaoEmpleadoImple : iDaoEmpleado
    {
        private BinaryWriter bwHeader;
        private BinaryWriter bwData;
        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 200;

        private List<HeaderIndex> headerIndices;
        private Header header;
        public DaoEmpleadoImple() { }

        private void Open()
        {
            try
            {
                headerIndices = new List<HeaderIndex>()
                {
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "IdEmpleado"
                    },
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "CedulaEmpleado"
                    },
                     new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "NombresEmpleado"
                    }
                };

                header = new Header()
                {
                    N = 0,
                    Name = "hEmpleado",
                    HeaderIndices = headerIndices
                };


                fsHeader = new FileStream("hempleado.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("dempleado.data", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if (fsHeader.Length == 0)
                {
                    bwHeader.Write(0);//n
                    bwHeader.Write(0);//k
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public List<Empleado> All()
        {
            List<Empleado> empleados = new List<Empleado>();
            try
            {

                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Empleado empleado = null;
                for(int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    empleado = new Empleado()
                    {
                        id = brData.ReadInt32(),
                        nombre = brData.ReadString(),
                        apellido = brData.ReadString(),
                        telefono = brData.ReadString(),
                        cedula = brData.ReadString(),
                        salario = brData.ReadDecimal(),
                        inss = brData.ReadString()
                    };

                    empleados.Add(empleado);
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return empleados;
        }

        public void Create(Empleado t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                long pos = k * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++k);
                bwData.Write(t.nombre);
                bwData.Write(t.apellido);
                bwData.Write(t.telefono);
                bwData.Write(t.cedula);
                bwData.Write(t.salario);
                bwData.Write(t.inss);
                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(++n);
                bwHeader.Write(k);

                long hpos = 8 + (n - 1) * 4;
                bwHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                bwHeader.Write(k);

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public bool Delete(Empleado t)
        {
            FileStream temporal = new FileStream("temporal.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);

            try
            {
                BinaryReader reader = new BinaryReader(temporal);
                BinaryWriter writer = new BinaryWriter(temporal);
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                writer.Write(n - 1);
                writer.Write(k);
                int j = 0;

                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int id = brHeader.ReadInt32();
                    if (id == t.id)
                    {
                        continue;
                    }
                    long tmpPos = 8 + 4 * j++;
                    writer.BaseStream.Seek(tmpPos, SeekOrigin.Begin);
                    writer.Write(id);
                }

                Close();
                File.Delete("hempleado.dat");
                writer.Close();
                reader.Close();
                temporal.Close();
                File.Move("temporal.dat", "hempleado.dat");
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

            return false;

        }

        public Empleado findByCedula(string cedula)
        {
            Empleado empleado = null;

            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dCedula = brData.ReadString();

                    if (!cedula.Equals(dCedula, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }

                    empleado = new Empleado()
                    {
                        id = brData.ReadInt32(),
                        nombre = brData.ReadString(),
                        apellido = brData.ReadString(),
                        telefono = brData.ReadString(),
                        cedula = brData.ReadString(),
                        salario = brData.ReadDecimal(),
                        inss = brData.ReadString(),

                    };
                    break;
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return empleado;
        }

        public Empleado findById(int id)
        {
            Empleado empleado = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                int index = Finder.BinarySearchById(brHeader, id, 0, n - 1);

                if(index < 0)
                {
                    return empleado;
                }

                long dpos = index * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                empleado = new Empleado()
                {
                    id = brData.ReadInt32(),
                    nombre = brData.ReadString(),
                    apellido = brData.ReadString(),
                    telefono = brData.ReadString(),
                    cedula = brData.ReadString(),
                    salario = brData.ReadDecimal(),
                    inss = brData.ReadString(),

                };

                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return empleado;
        }

        public Empleado findByNombre(string nombre)
        {
            throw new NotImplementedException();
        }

        public bool Update(Empleado t)
        {
            try
            {
                Open();
                long pos = ((t.id - 1) * SIZE);

                if (pos < 0)
                {
                    return false;
                }

                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(t.id);
                bwData.Write(t.nombre);
                bwData.Write(t.apellido);
                bwData.Write(t.telefono);
                bwData.Write(t.cedula);
                bwData.Write(t.salario);
                bwData.Write(t.inss);

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
            return true;
        }

        public int getGuardados()
        {
            Open();
            brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHeader.ReadInt32();
            int k = brHeader.ReadInt32();
            Close();
            return k;
        }
    }
}
