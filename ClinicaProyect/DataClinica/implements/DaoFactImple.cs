﻿using DataClinica.dao;
using DataClinica.entities;
using DataClinica.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataClinica.implements
{
    public class DaoFactImple : iDaoFactura
    {
        private BinaryWriter bwHeader;
        private BinaryWriter bwData;
        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 450;

        private List<HeaderIndex> headerIndices;
        private Header header;

        public DaoFactImple()
        {
        }
        private void Open()
        {
            try
            {
                headerIndices = new List<HeaderIndex>()
                {
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "IdFactura"
                    },
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "CodFactura"
                    }
                };

                header = new Header()
                {
                    N = 0,
                    Name = "hFactura",
                    HeaderIndices = headerIndices
                };


                fsHeader = new FileStream("hFactura.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("dFactura.data", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if (fsHeader.Length == 0)
                {
                    bwHeader.Write(0);//n
                    bwHeader.Write(0);//k
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public List<Factura> All()
        {
            List<Factura> facts = new List<Factura>();

            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Factura fact = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    fact = new Factura()
                    {
                        id = brData.ReadInt32(),
                        cod_factura = brData.ReadString(),
                        fecha = brData.ReadInt64(),
                        subtotal = brData.ReadDouble(),
                        iva = brData.ReadDouble(),
                        total = brData.ReadDouble(),
                        receta = brData.ReadString(),
                    };

                    facts.Add(fact);
                }

                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return facts;
        }

        public void Create(Factura t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                long pos = k * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++k);
                bwData.Write(t.cod_factura);
                bwData.Write(t.fecha);
                bwData.Write(t.subtotal);
                bwData.Write(t.iva);
                bwData.Write(t.total);
                bwData.Write(t.receta);

                long hpos = 8 + (n - 1) * 4;
                bwHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                bwHeader.Write(k);

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public bool Delete(Factura t)
        {
            FileStream temporal = new FileStream("temporal6.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);

            try
            {
                BinaryReader reader = new BinaryReader(temporal);
                BinaryWriter writer = new BinaryWriter(temporal);
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                writer.Write(n - 1);
                writer.Write(k);
                int j = 0;

                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int id = brHeader.ReadInt32();
                    if (id == t.id)
                    {
                        continue;
                    }
                    long tmpPos = 8 + 4 * j++;
                    writer.BaseStream.Seek(tmpPos, SeekOrigin.Begin);
                    writer.Write(id);
                }
                Close();
                File.Delete("hFactura.dat");
                writer.Close();
                reader.Close();
                temporal.Close();
                File.Move("temporal6.dat", "hFactura.dat");
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

            return false;
        }

        public Factura findByCod(string cod)
        {
            Factura fact = null;

            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dCod = brData.ReadString();

                    if(!cod.Equals(dCod, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }

                    fact = new Factura()
                    {
                        id = brData.ReadInt32(),
                        cod_factura = brData.ReadString(),
                        fecha = brData.ReadInt64(),
                        subtotal = brData.ReadDouble(),
                        iva = brData.ReadDouble(),
                        total = brData.ReadDouble(),
                        receta = brData.ReadString()
                    };
                    break;
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return fact;
        }

        public Factura findById(int id)
        {
            Factura fact = null;

            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                int index = Finder.BinarySearchById(brHeader, id, 0, n - 1);

                if (index < 0)
                {
                    return fact;
                }

                long dpos = index * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                fact = new Factura()
                {
                    id = brData.ReadInt32(),
                    cod_factura = brData.ReadString(),
                    fecha = brData.ReadInt64(),
                    subtotal = brData.ReadDouble(),
                    iva = brData.ReadDouble(),
                    total = brData.ReadDouble(),
                    receta = brData.ReadString()
                };
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return fact;
        }

        public bool Update(Factura t)
        {
            try
            {
                Open();
                long pos = ((t.id - 1) * SIZE);

                if (pos < 0)
                {
                    return false;
                }

                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(t.id);
                bwData.Write(t.cod_factura);
                bwData.Write(t.fecha);
                bwData.Write(t.subtotal);
                bwData.Write(t.iva);
                bwData.Write(t.total);
                bwData.Write(t.receta);


                Close();


            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
            return true;
        }

        public int getGuardados()
        {
            Open();
            brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brHeader.ReadInt32();
            int k = brHeader.ReadInt32();
            Close();
            return k;
        }
    }
}
