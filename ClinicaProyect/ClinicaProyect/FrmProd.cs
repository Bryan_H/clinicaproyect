﻿using DataClinica.entities;
using DataClinica.implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmProd : Form
    {
        public DataSet dsprod;
        private DaoProductoImple daoProductoImples;
        private DataRow drProd;
        private int index;
        private int idActualizar;
        public FrmProd()
        {
            daoProductoImples = new DaoProductoImple();
            InitializeComponent();
        }

        public void SetProd(Producto p, int index)
        {
            txtSku.Text = p.sku;
            txtNombre.Text = p.nombre;
            txtDescripcion.Text = p.descripcion;
            numericCantidad.Value = Int32.Parse(p.cantidad.ToString());
            txtPrecio.Text = p.precio.ToString();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            drProd = dsprod.Tables["Producto"].NewRow();

            Producto prod = new Producto()
            {
                id = daoProductoImples.getGuardados() + 1,
                sku = txtSku.Text,
                nombre = txtNombre.Text,
                descripcion = txtDescripcion.Text,
                cantidad = Convert.ToInt32(numericCantidad.Value),
                precio = Convert.ToDouble(txtPrecio.Text)
            };

            drProd["id"] = prod.id;
            drProd["sku"] = prod.sku;
            drProd["nombre"] = prod.nombre;
            drProd["descripcion"] = prod.descripcion;
            drProd["cantidad"] = prod.cantidad;
            drProd["precio"] = prod.precio;
            drProd["info"] = prod.sku + " -> " + prod.nombre;
            
            if(daoProductoImples.findBySku(prod.sku) != null)
            {
                MessageBox.Show("Error, SKU repetido", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            daoProductoImples.Create(prod);
            dsprod.Tables["Producto"].Rows.Add(drProd);
            Dispose();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            drProd = dsprod.Tables["Producto"].NewRow();

            Producto prod = new Producto()
            {
                id = daoProductoImples.getGuardados() + 1,
                sku = txtSku.Text,
                nombre = txtNombre.Text,
                descripcion = txtDescripcion.Text,
                cantidad = Convert.ToInt32(numericCantidad.Value),
                precio = Convert.ToDouble(txtPrecio.Text)
            };

            drProd["id"] = prod.id;
            drProd["sku"] = prod.sku;
            drProd["nombre"] = prod.nombre;
            drProd["descripcion"] = prod.descripcion;
            drProd["cantidad"] = prod.cantidad;
            drProd["precio"] = prod.precio;
            drProd["id"] = idActualizar;
            drProd["info"] = prod.sku + " -> " + prod.nombre;

            if (daoProductoImples.Update(prod))
            {
                MessageBox.Show("Se actualizo correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dsprod.Tables["Producto"].Rows.RemoveAt(index);
                dsprod.Tables["Producto"].Rows.InsertAt(drProd, index);
            }

            Dispose();
        }
    }
}
