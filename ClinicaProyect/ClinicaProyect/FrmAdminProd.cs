﻿using DataClinica.entities;
using DataClinica.implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmAdminProd : Form
    {
        private DaoProductoImple daoProductoImples;
        private List<Producto> prods;
        private BindingSource bsProds;
        private DataSet dsProd;

        public FrmAdminProd()
        {
            daoProductoImples = new DaoProductoImple();
            bsProds = new BindingSource();
            InitializeComponent();
        }

        public DataSet DsProd
        {
            set
            {
                dsProd = value;
            }
        }

        private void FrmAdminProd_Load(object sender, EventArgs e)
        {
            prods = daoProductoImples.All();
            dsProd.Tables["Producto"].Rows.Clear();

            prods.ForEach(p =>
            {
                dsProd.Tables["Producto"].Rows.Add(p.id, p.sku, p.nombre, p.descripcion, p.cantidad, p.precio);
            });

            bsProds.DataSource = dsProd;
            bsProds.DataMember = dsProd.Tables["Producto"].TableName;

            dgvProd.DataSource = bsProds;
            dgvProd.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmProd fp = new FrmProd();
            fp.dsprod = dsProd;
            fp.ShowDialog(this);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvProd.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvProd.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvProd.Rows[index].Cells["Id"].Value.ToString());
            string sku = dgvProd.Rows[index].Cells["SKU"].Value.ToString();
            string nombre = dgvProd.Rows[index].Cells["Nombre"].Value.ToString();
            string descripcion = dgvProd.Rows[index].Cells["Descripcion"].Value.ToString();
            int cantidad = Convert.ToInt32(dgvProd.Rows[index].Cells["Cantidad"].Value.ToString());
            double precio = Convert.ToDouble(dgvProd.Rows[index].Cells["Precio"].Value.ToString());

            Producto prod = new Producto()
            {
                id = id,
                sku = sku,
                nombre = nombre,
                descripcion = descripcion,
                cantidad = cantidad,
                precio = precio
            };

            FrmProd fp = new FrmProd();
            fp.SetProd(prod, index);
            fp.dsprod = dsProd;
            fp.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvProd.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder Eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvProd.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvProd.Rows[index].Cells["Id"].Value.ToString());
            string sku = dgvProd.Rows[index].Cells["SKU"].Value.ToString();
            string nombre = dgvProd.Rows[index].Cells["Nombre"].Value.ToString();
            string descripcion = dgvProd.Rows[index].Cells["Descripcion"].Value.ToString();
            int cantidad = Convert.ToInt32(dgvProd.Rows[index].Cells["Cantidad"].Value.ToString());
            double precio = Convert.ToDouble(dgvProd.Rows[index].Cells["Precio"].Value.ToString());

            Producto prod = new Producto()
            {
                id = id,
                sku = sku,
                nombre = nombre,
                descripcion = descripcion,
                cantidad = cantidad,
                precio = precio
            };

            DialogResult result = MessageBox.Show(this, "¿Desea eliminar ese registro?", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dgvProd.Rows.RemoveAt(index);
                daoProductoImples.Delete(prod);
            }
        }   
    }
}
