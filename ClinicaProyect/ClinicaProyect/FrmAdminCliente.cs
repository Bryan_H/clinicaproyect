﻿using DataClinica.entities;
using DataClinica.implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmAdminCliente : Form
    {
        private DaoClienteImple daoClienteImples;
        private List<Cliente> clientes;
        private BindingSource bsCliente;
        private DataSet dsCliente;

        public FrmAdminCliente()
        {
            daoClienteImples = new DaoClienteImple();
            bsCliente = new BindingSource();
            InitializeComponent();
        }

        public DataSet DsCliente
        {
            set
            {
                dsCliente = value;
            }
        }

        private void FrmAdminCliente_Load(object sender, EventArgs e)
        {
            clientes = daoClienteImples.All();
            dsCliente.Tables["Cliente"].Rows.Clear();

            clientes.ForEach(c =>
            {
                dsCliente.Tables["Cliente"].Rows.Add(c.id, c.nombre, c.apellido, c.telefono, c.cedula, c.direccion);
            });

            bsCliente.DataSource = dsCliente;
            bsCliente.DataMember = dsCliente.Tables["Cliente"].TableName;

            dgvCliente.DataSource = bsCliente;
            dgvCliente.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.dscliente = dsCliente;
            fc.setEnableButton(false, true);
            fc.ShowDialog(this);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCliente.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvCliente.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvCliente.Rows[index].Cells["Id"].Value.ToString());
            string nombre = dgvCliente.Rows[index].Cells["Nombre"].Value.ToString();
            string apellido = dgvCliente.Rows[index].Cells["Apellido"].Value.ToString();
            string telefono = dgvCliente.Rows[index].Cells["Telefono"].Value.ToString();
            string cedula = dgvCliente.Rows[index].Cells["Cedula"].Value.ToString();
            string direccion = dgvCliente.Rows[index].Cells["Direccion"].Value.ToString();

            Cliente cl = new Cliente()
            {
                id = id,
                nombre = nombre,
                apellido = apellido,
                telefono = telefono,
                cedula = cedula,
                direccion = direccion
            };

            FrmCliente fc = new FrmCliente();
            fc.SetCliente(cl, index);
            fc.dscliente = dsCliente;
            fc.setEnableButton(true, false);
            fc.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCliente.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder Eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvCliente.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvCliente.Rows[index].Cells["Id"].Value.ToString());
            string nombre = dgvCliente.Rows[index].Cells["Nombre"].Value.ToString();
            string apellido = dgvCliente.Rows[index].Cells["Apellido"].Value.ToString();
            string telefono = dgvCliente.Rows[index].Cells["Telefono"].Value.ToString();
            string cedula = dgvCliente.Rows[index].Cells["Cedula"].Value.ToString();
            string direccion = dgvCliente.Rows[index].Cells["Direccion"].Value.ToString();

            Cliente cl = new Cliente()
            {
                id = id,
                nombre = nombre,
                apellido = apellido,
                telefono = telefono,
                cedula = cedula,
                direccion = direccion
            };

            DialogResult result = MessageBox.Show(this, "¿Desea eliminar ese registro?", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dgvCliente.Rows.RemoveAt(index);
                daoClienteImples.Delete(cl);
            }
        }
    }
}
