﻿using DataClinica.entities;
using DataClinica.implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmDepto : Form
    {
        public DataSet dsdepto;
        private DaoDeptoImple daoDeptoImples;
        private DataRow drDepto;
        private int index;
        private int idActualizar;

        public FrmDepto()
        {
            daoDeptoImples = new DaoDeptoImple();
            InitializeComponent();
        }

        public void SetDepto(Depto d, int index)
        {
            txtNombre.Text = d.nombre;
            txtCodigo.Text = d.codigo;
            txtDescripcion.Text = d.descripcion;
        }

        public void setEnableButton(bool a, bool b)
        {
            btnActu.Enabled = a;
            btnGuardar.Enabled = b;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            drDepto = dsdepto.Tables["Depto"].NewRow();

            Depto depto = new Depto()
            {
                id = daoDeptoImples.getGuardados() + 1,
                nombre = txtNombre.Text,
                codigo = txtCodigo.Text,
                descripcion = txtDescripcion.Text
            };

            drDepto["id"] = depto.id;
            drDepto["nombre"] = depto.nombre;
            drDepto["codigo"] = depto.codigo;
            drDepto["descripcion"] = depto.descripcion;

            if(daoDeptoImples.findByCodigo(depto.codigo) != null)
            {
                MessageBox.Show("Error, Codigo repetida", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            daoDeptoImples.Create(depto);
            dsdepto.Tables["Depto"].Rows.Add(drDepto);
            Dispose();
        }

        private void btnActu_Click(object sender, EventArgs e)
        {
            drDepto = dsdepto.Tables["Depto"].NewRow();

            Depto depto = new Depto()
            {
                id = daoDeptoImples.getGuardados() + 1,
                nombre = txtNombre.Text,
                codigo = txtCodigo.Text,
                descripcion = txtDescripcion.Text
            };

            drDepto["id"] = depto.id;
            drDepto["nombre"] = depto.nombre;
            drDepto["codigo"] = depto.codigo;
            drDepto["descripcion"] = depto.descripcion;

            if (daoDeptoImples.Update(depto))
            {
                MessageBox.Show("Se actualizo correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dsdepto.Tables["Depto"].Rows.RemoveAt(index);
                dsdepto.Tables["Depto"].Rows.InsertAt(drDepto, index);
            }

            Dispose();
        }
    }
}
