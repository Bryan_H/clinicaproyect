﻿using DataClinica.entities;
using DataClinica.implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmFactura : Form
    {
        public DataSet dsfactura { get; set; }
        private DaoFactImple daoFactImples;
        private DataRow drFactura;
        private BindingSource bsFactura;

        private bool _canUpdate = true;
        private bool _needUpdate = false;
        private bool _canUpdatel = true;
        private bool _needUpdatel = false;
        private double subtotal;
        private double iva;
        private double total;
        public FrmFactura()
        {
            daoFactImples = new DaoFactImple();
            InitializeComponent();
        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleado.DataSource = dsfactura.Tables["Empleado"];
            cmbEmpleado.DisplayMember = "Nombre";
            cmbEmpleado.ValueMember = "Id";

            cmbCliente.DataSource = dsfactura.Tables["Cliente"];
            cmbCliente.DisplayMember = "Nombre";
            cmbCliente.ValueMember = "Id";

            cmbProd.DataSource = dsfactura.Tables["Producto"];
            cmbProd.DisplayMember = "Nombre";
            cmbProd.ValueMember = "Id";
            
        }

        private void UpdateData(string name)
        {
            if (name.Equals("Empleado"))
            {
                List<Empleado> searchData = dsfactura.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        id = dataRow.Field<Int32>("Id"),
                        nombre = dataRow.Field<string>("Nombre"),
                        apellido = dataRow.Field<string>("Apellido")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.nombre.Contains(cmbEmpleado.Text)), cmbEmpleado);

                if (cmbEmpleado.Text.Length > 1)
                {
                    searchData = dsfactura.Tables[name].AsEnumerable().Select(
                        dataRow =>
                        new Empleado
                        {
                            id = dataRow.Field<Int32>("Id"),
                            nombre = dataRow.Field<string>("Nombre"),
                            apellido = dataRow.Field<string>("Apellido")
                        }).ToList();
                }
                else
                {
                    RestartTimer();
                }
            }
            else if (name.Equals("Cliente"))
            {
                if (cmbEmpleado.Text.Length > 1)
                {
                    List<Cliente> searchData = dsfactura.Tables[name].AsEnumerable().Select(
                        dataRow =>
                        new Cliente
                        {
                            id = dataRow.Field<Int32>("Id"),
                            nombre = dataRow.Field<string>("Nombre"),
                            apellido = dataRow.Field<string>("Apellido")
                        }).ToList();

                    HandleTextChanged(searchData.FindAll(e =>
                    e.nombre.Contains(cmbCliente.Text)), cmbCliente);
                }
                else
                {
                    RestartTimer();
                }
            }
        }

        private void HandleTextChanged<T>(List<T> dataSource, ComboBox cmb)
        {
            var text = cmb.Text;

            if (dataSource.Count() > 0)
            {
                cmb.DataSource = dataSource;

                var sText = cmb.Items[0].ToString();
                cmb.SelectionStart = text.Length;
                cmb.SelectionLength = sText.Length - text.Length;
                cmb.DroppedDown = true;
                return;
            }
            else
            {
                cmb.DroppedDown = false;
                cmb.SelectionStart = text.Length;
            }
        }
        private void HandleTextChangedl(List<Producto> dataSource)
        {
            var text = cmbEmpleado.Text;

            if (dataSource.Count() > 0)
            {
                cmbEmpleado.DataSource = dataSource;

                var sText = cmbProd.Items[0].ToString();
                cmbProd.SelectionStart = text.Length;
                cmbProd.SelectionLength = sText.Length - text.Length;
                cmbProd.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleado.DroppedDown = false;
                cmbEmpleado.SelectionStart = text.Length;
            }
        }
        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        private void cmbEmpleado_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData("Empleado");
                }
                else
                {
                    RestartTimer();
                }
            }
        }

        private void cmbEmpleado_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void cmbEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void Timer1_Tick_1(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData("Empleado");
            UpdateData("Clientes");
        }

        private void cmbProd_TextUpdate(object sender, EventArgs e)
        {
            MessageBox.Show(cmbProd.SelectedValue.ToString());
        }

        private void cmbProd_TextChanged(object sender, EventArgs e)
        {

        }

        private void UpdateDatal()
        {

            if (cmbProd.Text.Length > 1)
            {

                List<Producto> searchData = dsfactura.Tables["Producto"].AsEnumerable().Select(
                    dataRow =>
                    new Producto
                    {
                        id = dataRow.Field<Int32>("Id"),
                        sku = dataRow.Field<String>("Sku"),
                        nombre = dataRow.Field<String>("Nombre"),



                    }).ToList();

                HandleTextChangedl(searchData.FindAll(e => e.nombre.Contains(cmbProd.Text)));

            }
            else
            {
                RestartTimeri();
            }
        }

        private void RestartTimeri()
        {
            timer2.Stop();
            _canUpdatel = false;
            timer2.Start();
        }

        private void cmbProd_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdatel = false;

            txtCant.Text = ((DataRowView)cmbProd.SelectedItem).Row["Cantidad"].ToString();
            txtPrecio.Text = ((DataRowView)cmbProd.SelectedItem).Row["Precio"].ToString();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow ProductoFactura = dsfactura.Tables["ProductoFactura"].NewRow();
                drFactura["Sku"] = ((DataRowView)cmbProd.SelectedItem).Row["Sku"].ToString();
                drFactura["Nombre"] = ((DataRowView)cmbProd.SelectedItem).Row["Nombre"].ToString();
                drFactura["Cantidad"] = 0;
                drFactura["Precio"] = Double.Parse(((DataRowView)cmbProd.SelectedItem).Row["Precio"].ToString());
                drFactura["ID"] = Int32.Parse(((DataRowView)cmbProd.SelectedItem).Row["Id"].ToString());

                dsfactura.Tables["ProductoFactura"].Rows.Add(drFactura);

                bsFactura.DataSource = dsfactura;
                bsFactura.DataMember = "ProductoFactura";

                dgvFact.DataSource = bsFactura;
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "Duplicado, producto ya se encuentra agregado", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void calcularTotal()
        {
            subtotal = 0; iva = 0; total = 0;
            int i = 0;
            foreach (DataGridViewRow row in dgvFact.Rows)
            {
                subtotal += double.Parse(row.Cells[3].Value.ToString()) * double.Parse(row.Cells[2].Value.ToString());
                dsfactura.Tables["ProductoFactura"].Rows[i++]["Cantidad"] = double.Parse(row.Cells[2].Value.ToString());
            }

            iva = subtotal * 0.15;
            total = subtotal + iva;

            txtSubTotal.Text = subtotal.ToString();
            txtIVA.Text = iva.ToString();
            txtTotal.Text = total.ToString();

        }

        private void dgvFact_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int cantidadProductoCell = Int32.Parse(dgvFact.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
            int indexProductoCell = Int32.Parse(dsfactura.Tables["ProductoFactura"].Rows[e.RowIndex]["Id"].ToString());
            DataRow drProductoStored = dsfactura.Tables["Producto"].Rows.Find(indexProductoCell);
            int cantidadProductoStored = Int32.Parse(drProductoStored["Cantidad"].ToString());
            //Validando que la cantidad en el almacen sea mayor o igual que la que se quiere vender
            if (cantidadProductoCell > cantidadProductoStored)
            {
                dgvFact.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                MessageBox.Show(this, "La cantidad no puede exceder la indicada en el almacen", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //si las cantidades estan correctas se calcula el subtotal, iva y total
            calcularTotal();
        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            int rowCount = dgvFact.Rows.Count;
            //validando si se agregaron productos al datagridview
            if (rowCount <= 0)
            {
                MessageBox.Show(this, "Debe de agregar al menos un producto a factura", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //validando si alguna de los productos agregados al datagridview tiene en la columna Cantidad cero
            Boolean flag = false;
            foreach (DataGridViewRow row in dgvFact.Rows)
            {
                int cantidad = Int32.Parse(row.Cells["Cantidad"].Value.ToString());
                if (cantidad <= 0)
                {
                    flag = true;
                    break;
                }

            }

            if (flag)
            {
                MessageBox.Show(this, "La cantidad de un producto no puede ser cero (0)", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsfactura.Tables["Factura"].NewRow();
            drFactura["Id"] = dsfactura.Tables["Factura"].Rows.Count + 1;
            drFactura["Cod_Factura"] = "F" + dsfactura.Tables["Factura"].Rows.Count + 1;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Subtotal"] = subtotal;
            drFactura["IVA"] = iva;
            drFactura["Total"] = total;            
            drFactura["Receta"] = txtReceta.Text;

            dsfactura.Tables["Factura"].Rows.Add(drFactura);


            foreach (DataRow drPF in dsfactura.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsfactura.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Id"] = dsfactura.Tables["DetalleFactura"].Rows.Count + 1;
                drDetalleFactura["Factura"] = drFactura["Id"];
                drDetalleFactura["Producto"] = drPF["Id"];
                drDetalleFactura["Cantidad"] = drPF["Cantidad"];
                drDetalleFactura["Precio"] = drPF["Precio"];

                dsfactura.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);
            }

            DataRow g = dsfactura.Tables["Empleado"].NewRow();
            g["Nombres"] = cmbEmpleado.SelectedValue.ToString();
            dsfactura.Tables["Empleado"].Rows.Add(g);

            ReportFactura fr = new ReportFactura();
            fr.DsSistema = dsfactura;


            fr.Show();

            Dispose();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rows = dgvFact.SelectedRows;
            if (rows.Count <= 0)
            {
                MessageBox.Show(this, "No hay filas para eliminar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow dgvrow = rows[0];

            DataRow drProductoToDelete = ((DataRowView)dgvrow.DataBoundItem).Row;

            dsfactura.Tables["ProductoFactura"].Rows.Remove(drProductoToDelete);
        }

        private void cmbCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void cmbCliente_TextUpdate(object sender, EventArgs e)
        {
            MessageBox.Show(cmbCliente.SelectedValue.ToString());
        }

        private void cmbCliente_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData("Cliente");
                }
                else
                {
                    RestartTimer();
                }
            }
        }
    }
}
