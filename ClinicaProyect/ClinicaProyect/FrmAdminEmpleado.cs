﻿using DataClinica.entities;
using DataClinica.implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmAdminEmpleado : Form
    {
        private DaoEmpleadoImple daoEmpleadosImples;
        private List<Empleado> empleados;
        private BindingSource bsEmpleados;
        private DataSet dsClinica;

        public FrmAdminEmpleado()
        {
            daoEmpleadosImples = new DaoEmpleadoImple();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        public DataSet DsClinica
        {
            set
            {
                dsClinica = value;
            }
        }

        private void FrmAdminEmpleado_Load(object sender, EventArgs e)
        {
            empleados = daoEmpleadosImples.All();
            dsClinica.Tables["Empleado"].Rows.Clear();

            empleados.ForEach(ep =>
            {
                dsClinica.Tables["Empleado"].Rows.Add(ep.id, ep.nombre, ep.apellido, ep.telefono, ep.cedula, ep.salario, ep.inss);
            });

            bsEmpleados.DataSource = dsClinica;
            bsEmpleados.DataMember = dsClinica.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmEmpleado fe = new FrmEmpleado();
            fe.DSClinica = dsClinica;
            fe.setEnableButton(false, true);
            fe.ShowDialog(this);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvEmpleados.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
            string nombre = dgvEmpleados.Rows[index].Cells["Nombre"].Value.ToString();
            string apellido = dgvEmpleados.Rows[index].Cells["Apellido"].Value.ToString();
            string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
            string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
            decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());
            string inss = dgvEmpleados.Rows[index].Cells["INSS"].Value.ToString();

            Empleado empleado = new Empleado()
            {
                id = id,
                nombre = nombre,
                apellido = apellido,
                telefono = telefono,
                cedula = cedula,
                salario = salario,
                inss = inss
            };

            

            FrmEmpleado fe = new FrmEmpleado();
            fe.setEmpleado(empleado, index);
            fe.DSClinica = dsClinica;
            fe.setEnableButton(true, false);
            fe.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder Eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvEmpleados.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
            string nombre = dgvEmpleados.Rows[index].Cells["Nombre"].Value.ToString();
            string apellido = dgvEmpleados.Rows[index].Cells["Apellido"].Value.ToString();
            string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
            string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
            decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());
            string inss = dgvEmpleados.Rows[index].Cells["INSS"].Value.ToString();

            Empleado empleado = new Empleado()
            {
                id = id,
                nombre = nombre,
                apellido = apellido,
                telefono = telefono,
                cedula = cedula,
                salario = salario,
                inss = inss
            };

            DialogResult result = MessageBox.Show(this, "¿Desea eliminar ese registro?", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if(result == DialogResult.Yes)
            {
                dgvEmpleados.Rows.RemoveAt(index);
                daoEmpleadosImples.Delete(empleado);
            }

        }
    }
}
