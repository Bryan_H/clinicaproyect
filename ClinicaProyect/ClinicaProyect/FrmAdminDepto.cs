﻿using DataClinica.entities;
using DataClinica.implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmAdminDepto : Form
    {
        private DaoDeptoImple daoDeptoImples;
        private List<Depto> deptos;
        private BindingSource bsDepto;
        private DataSet dsDepto;

        public FrmAdminDepto()
        {
            daoDeptoImples = new DaoDeptoImple();
            bsDepto = new BindingSource();
            InitializeComponent();
        }

        public DataSet DsDepto
        {
            set
            {
                dsDepto = value;
            }
        }

        private void FrmAdminDepto_Load(object sender, EventArgs e)
        {
            deptos = daoDeptoImples.All();
            dsDepto.Tables["Depto"].Rows.Clear();

            deptos.ForEach(d =>
            {
                dsDepto.Tables["Depto"].Rows.Add(d.id, d.nombre, d.codigo, d.descripcion);
            });

            bsDepto.DataSource = dsDepto;
            bsDepto.DataMember = dsDepto.Tables["Depto"].TableName;

            dgvDepto.DataSource = bsDepto;
            dgvDepto.AutoGenerateColumns = true;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmDepto fd = new FrmDepto();
            fd.dsdepto = dsDepto;
            fd.setEnableButton(false, true);
            fd.ShowDialog(this);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvDepto.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvDepto.SelectedRows[0].Index;

            Depto d = new Depto()
            {
                id = Convert.ToInt32(dgvDepto.Rows[index].Cells["Id"].Value.ToString()),
                nombre = dgvDepto.Rows[index].Cells["Nombre"].Value.ToString(),
                codigo = dgvDepto.Rows[index].Cells["Codigo"].Value.ToString(),
                descripcion = dgvDepto.Rows[index].Cells["Descripcion"].Value.ToString(),
            };

            FrmDepto fd = new FrmDepto();
            fd.SetDepto(d, index);
            fd.dsdepto = dsDepto;
            fd.setEnableButton(true, false);
            fd.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvDepto.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder Eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = dgvDepto.SelectedRows[0].Index;

            Depto d = new Depto()
            {
                id = Convert.ToInt32(dgvDepto.Rows[index].Cells["Id"].Value.ToString()),
                nombre = dgvDepto.Rows[index].Cells["Nombre"].Value.ToString(),
                codigo = dgvDepto.Rows[index].Cells["Codigo"].Value.ToString(),
                descripcion = dgvDepto.Rows[index].Cells["Descripcion"].Value.ToString(),
            };

            DialogResult result = MessageBox.Show(this, "¿Desea eliminar ese registro?", "Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dgvDepto.Rows.RemoveAt(index);
                daoDeptoImples.Delete(d);
            }
        }
    }
}
