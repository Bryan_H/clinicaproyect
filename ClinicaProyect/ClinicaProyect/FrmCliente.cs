﻿using DataClinica.entities;
using DataClinica.implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmCliente : Form
    {
        public DataSet dscliente { get; set; }
        private DaoClienteImple daoClienteImples;
        private DataRow drCliente;
        private int index;
        private int idActualizar;

        public FrmCliente()
        {
            daoClienteImples = new DaoClienteImple();
            InitializeComponent();
        }

        public void SetCliente(Cliente c, int index)
        {
            txtNombre.Text = c.nombre;
            txtApellido.Text = c.apellido;
            txtTelefono.Text = c.telefono;
            txtCedula.Text = c.cedula;
            txtDireccion.Text = c.direccion;
        }

        public void setEnableButton(bool a, bool b)
        {
            btnActu.Enabled = a;
            btnGuardar.Enabled = b;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            drCliente = dscliente.Tables["Cliente"].NewRow();

            Cliente cl = new Cliente()
            {
                id = daoClienteImples.GetGuardados() + 1,
                nombre = txtNombre.Text,
                apellido = txtApellido.Text,
                telefono = txtTelefono.Text,
                cedula = txtCedula.Text,
                direccion = txtDireccion.Text
            };

            drCliente["id"] = cl.id;
            drCliente["nombre"] = cl.nombre;
            drCliente["apellido"] = cl.apellido;
            drCliente["telefono"] = cl.telefono;
            drCliente["cedula"] = cl.cedula;
            drCliente["direccion"] = cl.direccion;
            drCliente["na"] = cl.nombre + " " + cl.apellido + "--" + cl.cedula;

            if(daoClienteImples.findByCedula(cl.cedula) != null)
            {
                MessageBox.Show("Error, Cedula repetida", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            daoClienteImples.Create(cl);
            dscliente.Tables["Cliente"].Rows.Add(drCliente);
            Dispose();

        }

        private void btnActu_Click(object sender, EventArgs e)
        {
            drCliente = dscliente.Tables["Cliente"].NewRow();

            Cliente cl = new Cliente()
            {
                id = daoClienteImples.GetGuardados() + 1,
                nombre = txtNombre.Text,
                apellido = txtApellido.Text,
                telefono = txtTelefono.Text,
                cedula = txtCedula.Text,
                direccion = txtDireccion.Text
            };

            drCliente["id"] = cl.id;
            drCliente["nombre"] = cl.nombre;
            drCliente["apellido"] = cl.apellido;
            drCliente["telefono"] = cl.telefono;
            drCliente["cedula"] = cl.cedula;
            drCliente["direccion"] = cl.direccion;
            drCliente["na"] = cl.nombre + " " + cl.apellido + "--" + cl.cedula;

            if (daoClienteImples.Update(cl))
            {
                MessageBox.Show("Se actualizo correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dscliente.Tables["Cliente"].Rows.RemoveAt(index);
                dscliente.Tables["Cliente"].Rows.InsertAt(drCliente, index);
            }

            Dispose();
        }
    }
}
