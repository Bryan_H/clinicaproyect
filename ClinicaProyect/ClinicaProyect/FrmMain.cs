﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmMain : Form
    {
        
        public FrmMain()
        {
            InitializeComponent();
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAdminEmpleado fae = new FrmAdminEmpleado();
            fae.MdiParent = this;
            fae.DsClinica = dsClinica;
            fae.Show();
            
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAdminCliente fac = new FrmAdminCliente();
            fac.MdiParent = this;
            fac.DsCliente = dsClinica;
            fac.Show();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAdminProd fap = new FrmAdminProd();
            fap.MdiParent = this;
            fap.DsProd = dsClinica;
            fap.Show();
        }

        private void departamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAdminDepto fad = new FrmAdminDepto();
            fad.MdiParent = this;
            fad.DsDepto = dsClinica;
            fad.Show();

        }

        private void facturaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.dsfactura = dsClinica;
            ff.Show();
        }
    }
}
