﻿using DataClinica.entities;
using DataClinica.implements;
using DataClinica.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class FrmEmpleado : Form
    {
        public DataSet DSClinica { get; set; }
        private DaoEmpleadoImple daoEmpeladoImples;
        private DataRow drEmpleado;
        private int index;
        private int idActualizar;

        public FrmEmpleado()
        {
            daoEmpeladoImples = new DaoEmpleadoImple();
            InitializeComponent();
        }

        public void setEmpleado(Empleado e, int index)
        {
            txtNombre.Text = e.nombre;
            txtApellido.Text = e.apellido;
            mtxtTelefono.Text = e.telefono;
            mtxtCedula.Text = e.cedula;
            txtSalario.Text = e.salario.ToString();
            mtxtINSS.Text = e.inss;
            this.index = index;
            this.idActualizar = e.id;
        }

        public void setEnableButton(bool flag1, bool flag2)
        {
            btnActu.Enabled = flag1;
            btnGuardar.Enabled = flag2;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            drEmpleado = DSClinica.Tables["Empleado"].NewRow();
            Empleado empleado = new Empleado()
            {
                id = daoEmpeladoImples.getGuardados()+1,
                nombre = txtNombre.Text,
                apellido = txtApellido.Text,
                telefono = mtxtTelefono.Text,
                cedula = mtxtCedula.Text,
                salario = Convert.ToDecimal(txtSalario.Text),
                inss = mtxtINSS.Text
            };

            drEmpleado["id"] = empleado.id;
            drEmpleado["nombre"] = empleado.nombre;
            drEmpleado["apellido"] = empleado.apellido;
            drEmpleado["telefono"] = empleado.telefono;
            drEmpleado["cedula"] = empleado.cedula;
            drEmpleado["salario"] = empleado.salario;
            drEmpleado["inss"] = empleado.inss;
            drEmpleado["na"] = empleado.nombre + " " + empleado.apellido;

            if(daoEmpeladoImples.findByCedula(empleado.cedula) != null)
            {
                MessageBox.Show("Error, Cedula repetida", "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            daoEmpeladoImples.Create(empleado);
            DSClinica.Tables["Empleado"].Rows.Add(drEmpleado);
            Dispose();
        }

        private void btnActu_Click(object sender, EventArgs e)
        {
            drEmpleado = DSClinica.Tables["Empleado"].NewRow();

            Empleado empleado = new Empleado()
            {
                id = daoEmpeladoImples.getGuardados() + 1,
                nombre = txtNombre.Text,
                apellido = txtApellido.Text,
                telefono = mtxtTelefono.Text,
                cedula = mtxtCedula.Text,
                salario = Convert.ToDecimal(txtSalario.Text),
                inss = mtxtINSS.Text
            };

            drEmpleado["nombre"] = empleado.nombre;
            drEmpleado["apellido"] = empleado.apellido;
            drEmpleado["telefono"] = empleado.telefono;
            drEmpleado["cedula"] = empleado.cedula;
            drEmpleado["salario"] = empleado.salario;
            drEmpleado["inss"] = empleado.inss;
            drEmpleado["na"] = empleado.nombre + " " + empleado.apellido;
            drEmpleado["id"] = idActualizar;

            if (daoEmpeladoImples.Update(empleado))
            {
                MessageBox.Show("Se actualizo correctamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DSClinica.Tables["Empleado"].Rows.RemoveAt(index);
                DSClinica.Tables["Empleado"].Rows.InsertAt(drEmpleado, index);
            }
            else
            {
                MessageBox.Show("No se pudo actualizar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            Dispose();
        }
        private void LetterOnly(object sender, KeyPressEventArgs e)
        {
            Validator.SoloLetras(e);
        }

        private void ForMoney(object sender, KeyPressEventArgs e)
        {
            Validator.NumerosDecimales(e);
        }
    }
}
