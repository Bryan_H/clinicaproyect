﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClinicaProyect
{
    public partial class ReportFactura : Form
    {
        private DataSet dsSistema;
        private BindingSource bsFactura;
        private BindingSource bsProductoFactura;
        private BindingSource bsNombre;
        public ReportFactura()
        {
            InitializeComponent();
            bsFactura = new BindingSource();
            bsProductoFactura = new BindingSource();
            bsNombre = new BindingSource();
        }

        public DataSet DsSistema { set => dsSistema = value; }

        private void ReportFactura_Load(object sender, EventArgs e)
        {
            bsFactura.DataSource = dsSistema;
            bsFactura.DataMember = "Factura";
            bsProductoFactura.DataSource = dsSistema;
            bsProductoFactura.DataMember = "ProductoFactura";
            bsProductoFactura.DataSource = dsSistema;
            bsProductoFactura.DataMember = "Empleado";

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ClinicaProyect.FacturaReport.rdlc";
            ReportDataSource rds3 = new ReportDataSource("DSEmpleado", dsSistema.Tables["Empleado"]);
            ReportDataSource rds1 = new ReportDataSource("DSFactura", dsSistema.Tables["Factura"]);
            ReportDataSource rds2 = new ReportDataSource("DSProductoFactura", dsSistema.Tables["ProductoFactura"]);
            ReportDataSource rds4 = new ReportDataSource("DSCliente", dsSistema.Tables["Cliente"]);
            this.reportViewer1.LocalReport.DataSources.Clear();
           /* this.reportViewer1.LocalReport.DataSources.Add(rds1);
            this.reportViewer1.LocalReport.DataSources.Add(rds2);
            this.reportViewer1.LocalReport.DataSources.Add(rds3);
            this.reportViewer1.LocalReport.DataSources.Add(rds4);
           */

            this.reportViewer1.RefreshReport();
        }
    }
}
